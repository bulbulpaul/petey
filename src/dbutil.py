# coding:utf-8

import datetime
import time

import pymysql
from sqlalchemy import func
from sqlalchemy.engine import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import validates, sessionmaker
from sqlalchemy.pool import QueuePool
from sqlalchemy.schema import Column, ForeignKey
from sqlalchemy.types import Integer, String, DateTime, Boolean
from sqlalchemy.dialects.mysql import TEXT
from werkzeug.security import generate_password_hash, check_password_hash
from hoedown import Markdown, HtmlRenderer
from hashids import Hashids


Base = declarative_base()


class DB():

    def get_session(self):
        Session = sessionmaker(self.engine)
        return Session()

    def init_connector(self):
        return pymysql.connect(host=self.host, port=self.port, user=self.user, passwd=self.passwd,
                               db=self.db, charset='utf8', use_unicode='true')

    def __init__(self, host, port, user, passwd, db):
        self.host = host
        self.port = port
        self.user = user
        self.passwd = passwd
        self.db = db
        self.engine = create_engine('mysql+pymysql://', creator=self.init_connector,
                                    poolclass=QueuePool, pool_size=500, max_overflow=50)

        # MarkdownParser
        renderer = HtmlRenderer()
        self.md_parser = Markdown(renderer)

        # id generate
        self.hashids = Hashids(salt='p0e2t1e5y', min_length=16)

    def generate_user_time_hash(self, user_id):
        return self.hashids.encode(int(''.join([str(ord(s)) for s in user_id])), int(time.time()))

    def auth_user(self, user, passwd):
        s = self.get_session()
        user = s.query(User).filter_by(user_id=user).first()
        s.close()
        return user is not None and user.check_password(passwd)

    def create_user(self, user_id, name, passwd, email):
        s = self.get_session()
        user = User(user_id, name, passwd, email)
        s.add(user)
        s.commit()
        return True

    def compile_text(self, text, doc_type="md"):
        html = None
        if doc_type == "md":
            html = self.md_parser.render(text)
        return html

    def create_article(self, title, user_id, text, text_type='md', tags=None):
        s = self.get_session()
        a_id = self.generate_user_time_hash(user_id)
        article = Article(a_id, title, user_id, text,
                          text_type, self.compile_text(text, text_type), tags)
        s.add(article)
        s.commit()
        s.close()
        return a_id

    def select_article(self, user_id, article_id):
        s = self.get_session()
        article = s.query(Article).filter_by(id=article_id, user_id=user_id).first()
        s.close()
        return article

    def select_article_list(self, limit, offset=0, sort='time',tag=None, keywords=None):
        s = self.get_session()
        if sort == 'time':
            # TODO support desc
            articles = s.query(Article).order_by(Article.last_update.desc()).slice(offset, limit)
        else:
            articles = s.query(Article).order_by(Article.last_update.desc())
        s.close()
        return articles

    def select_draft_article(self, user_id, article_id):
        s = self.get_session()
        article = s.query(Article).filter_by(id=article_id, user_id=user_id).first()
        s.close()
        return article

    def update_article(self, article_id, title, text, text_type='md', tags=None):
        s = self.get_session()
        article = s.query(Article).filter_by(id=article_id).first()
        if article is None:
            pass # TODO Error処理
        article.title = title
        article.raw = text
        article.type = text_type
        article.tags = tags
        s.commit()
        s.close()
        return True

    def create_db(self):
        Base.metadata.create_all(self.engine)


class User(Base):
    __tablename__ = 'users'

    user_id = Column(String(255), primary_key=True)
    name = Column(String(255), nullable=False)
    email = Column(String(255), nullable=False)
    passwd = Column(String(255), nullable=False)
    create_date = Column(DateTime, nullable=False)
    active = Column(Boolean)
    login_date = Column(DateTime)
    badge = Column(String(255))

    def __init__(self, user_id, name, passwd, email):
        self.user_id = user_id
        self.name = name
        self.email = email
        self.passwd = generate_password_hash(passwd)
        self.create_date = datetime.datetime.now()
        self.active = False

    def check_password(self, passwd):
        return check_password_hash(self.passwd, passwd)

    @validates('email')
    def validate_email(self, key, email):
        assert '@' in email
        return email


class Article(Base):
    __tablename__ = 'article'

    id = Column(String(255), primary_key=True)
    title = Column(String(255), nullable=False)
    user_id = Column(String(255), ForeignKey('users.user_id'))
    raw = Column(TEXT, nullable=False)
    type = Column(String(255), nullable=False)
    html = Column(TEXT, nullable=False)
    tags = Column(String(255))
    last_update = Column(DateTime, nullable=False)
    active = Column(Boolean)

    def __init__(self, id, title, user_id, raw, type, html, tags=None):
        self.id = id
        self.title = title
        self.user_id = user_id
        self.raw = raw
        self.type = type
        self.html = html
        self.tags = tags
        self.last_update = datetime.datetime.now()
        self.active = True


if __name__ == '__main__':
    db = DB('192.168.168.168', 3306, 'petey', 'petey', 'Petey')
    db.create_db()
    db.create_user('admin', 'admin', 'admin123', 'admin@example.com')
    assert db.auth_user('admin', 'admin123')

