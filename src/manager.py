# coding:utf-8
import yaml


class Manager:

    def __init__(self):
        self.settings = yaml.load()
        f = open('../config/config.yml', 'r')
        settings = yaml.load(f)
        self.db_host = settings['db_host']
        self.db_port = settings['db_port']
        self.db_name = settings['db_name']
        self.db_user = settings['db_user']
        self.db_passwd = settings['db_passwd']
        f.close()
