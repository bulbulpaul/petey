# coding:utf-8
import re

from werkzeug.contrib.cache import SimpleCache
from flask import Flask, request, render_template, session, redirect, url_for
from flask_mail import Mail, Message
from jinja2 import FileSystemLoader
from hoedown import Markdown, HtmlRenderer

from dbutil import DB
import manager

app = Flask(__name__)
# app.jinja_loader = FileSystemLoader('resource/template')
app.config['SECRET_KEY'] = 'R%Jft!6iKdEQmIt*02&aj3IqjoQ8kLdd'
mail = Mail(app)

# MarkdownParser
renderer = HtmlRenderer()
md_parser = Markdown(renderer)

# manager =　Manager()
db = DB('192.168.168.168', 3306, 'petey', 'petey', 'Petey')

# cashe
cache = SimpleCache()

@app.before_request
def login_check():
    if session.get('user_id') is not None:
        return
    # リクエストがログインページに関するもの
    if request.path == '/login' or '/static/' or '/signup' in request.path:
        return
    # ログインされておらずログインページに関するリクエストでもなければリダイレクトする
    return redirect('/login')

@app.route('/')
def index():
    articles = db.select_article_list(10)
    return render_template('index.html', articles=articles)

@app.route('/signup', methods=['POST', 'GET'])
def signup():
    if request.method == 'POST':
        user_data = {}
        user_data['user'] = request.form['user']
        user_data['email'] = request.form['email']
        user_data['passwd'] = request.form['passwd']
        confirm = request.form['confirm']
        if user_data['passwd'] == confirm:
            # セッションへトークン保存
            token = db.generate_user_time_hash(request.form['user'])
            msg = Message("Welcome to Petey",
                          sender="no-reply@petey.com",
                          recipients=[request.form['email']])
            msg.body = 'Thank you for registration! ¥n 登録ありがとう! ¥n http://localhost:5000/signup/' + token
            msg.html = "<h1>Thank you for registration!</h1><p>登録ありがとう!http://localhost:5000/signup/" + token + "</p>"
            mail.send(msg)
            cache.set(token, user_data, timeout=5 * 60)
            return render_template('presignup.html')
        else:
            pass

    return render_template('signup.html')

@app.route('/signup/<auth_token>')
def signup_check(auth_token):
    # 認証トークンの確認
    user_data = cache.get(auth_token)
    print(str(user_data))
    url = None
    if user_data is None:
        # TODO エラー
        pass
    else:
        # 認証確認
        db.create_user(user_data['user'], user_data['user'], user_data['passwd'],user_data['email'], )
        cache.delete(auth_token)
        url = 'signup_success.html'
    return render_template(url)

@app.route('/<user_id>/items/<item_id>')
def item(user_id, item_id):
    article = db.select_article(user_id, item_id)
    return render_template('article.html', article=article)

@app.route('/drafts/<draft_id>', methods=['POST', 'GET'])
def drafts(draft_id):
    url = 'edit.html'
    param = {}
    if request.method == 'GET':
        if draft_id == 'new':
            url = 'edit.html'
        else:
            # 既存記事の編集
            article = db.select_article(session.get('user_id'), draft_id)
            param['title'] = article['title']
            param['raw'] = article['raw']
            param['tags'] = article['tags']
            param['html'] = article['html']

        return render_template(url)

    elif request.method == 'POST':

        title = request.form['title']
        text = request.form['text']
        tags = request.form['tag'] if len(request.form['tag']) == 0 else None
        if draft_id == 'new':
            # insert
            a_id = db.create_article(title, session.get('user_id'), text, 'md', tags)
            url = '/' + session.get('user_id') + '/items/' + a_id
        else:
            # update
            db.update_article(draft_id, title, text, 'md', tags)
            url = '/' + session.get('user_id') + '/items/' + draft_id
        return redirect(url)

    return render_template(url)

@app.route('/user/<user_id>')
def user(user_id):
    pass

@app.route('/api/<func_name>', methods=['POST'])
def api(func_name):
    if func_name == 'parse':
        text = request.form['text']
        return parse_md(text)

@app.route('/register')
def register():
    return

@app.route('/login', methods=['POST', 'GET'])
def login():
    if request.method == 'POST' and db.auth_user(request.form['user'], request.form['passwd']):
        session['user_id'] = request.form['user']
        print('user' + request.form['user'])
        return redirect('/')
    else:
        return render_template('login.html', auth=False)

@app.route('/logout', methods=['GET'])
def logout():
    #セッションからユーザ名を取り除く (ログアウトの状態にする)
    session.pop('username', None)
    # ログインページにリダイレクトする
    return redirect(url_for('login'))

@app.errorhandler(404)
def page_not_found(e):
    return render_template('404.html'), 404


def parse_md(md):
    return md_parser.render(md)


def validate_character(value):
    if re.match('[^a-zA-Z0-9_.\.\-]', value) is not None:
        pass

if __name__ == '__main__':
    app.run(debug=True)